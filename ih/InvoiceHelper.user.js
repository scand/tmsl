// ==UserScript==
// @name        InvoiceHelper
// @namespace   invoices.phoebius.com
// @version     0.0.3
// @description Brings additional links to invoices.phoebius.com service as Tampermonkey script
// @author      tch@scand.com
// @match       https://invoices.phoebius.com/issues/*
// @grant       GM_log
// ==/UserScript==

(function() {
    'use strict';

    const customerFieldId = 'cf_3';
    const nickFieldId = 'cf_14';

    const invBaseUrl = 'https://invoices.phoebius.com';
    const customerRatesTarget = invBaseUrl + '/projects/rates/issues?c%5B%5D=status&c%5B%5D=cf_3&c%5B%5D=subject&c%5B%5D=cf_14&c%5B%5D=cf_12&c%5B%5D=cf_13&c%5B%5D=cf_9&c%5B%5D=cf_10&f%5B%5D=status_id&f%5B%5D=cf_3&f%5B%5D=&per_page=100&group_by=&op%5Bcf_3%5D=%3D&op%5Bstatus_id%5D=o&set_filter=1&sort=cf_12%2Cparent%3Adesc%2Csubject&utf8=%E2%9C%93&v%5Bcf_3%5D%5B%5D=';
    const customerIvoiceRatesTarget = invBaseUrl + '/projects/rates/issues?utf8=%E2%9C%93&f%5B%5D=&c%5B%5D=cf_12&c%5B%5D=subject&c%5B%5D=cf_14&c%5B%5D=cf_13&c%5B%5D=cf_9&c%5B%5D=cf_10&per_page=100&group_by=tracker&set_filter=1&f%5B%5D=status_id&op%5Bstatus_id%5D=%3D&v%5Bstatus_id%5D%5B%5D=16&f%5B%5D=tracker_id&op%5Btracker_id%5D=%3D&v%5Btracker_id%5D%5B%5D=6&f%5B%5D=cf_9&op%5Bcf_9%5D=%3C%3D&v%5Bcf_9%5D%5B%5D=${dateStart}&f%5B%5D=cf_10&op%5Bcf_10%5D=%3E%3D&v%5Bcf_10%5D%5B%5D=${dateEnd}&f%5B%5D=cf_3&op%5Bcf_3%5D=%3D&v%5Bcf_3%5D%5B%5D=';
    const workerRatesTarget = invBaseUrl + '/projects/rates/issues?f[]=&c[]=status&c[]=cf_3&c[]=subject&c[]=cf_9&c[]=cf_10&c[]=cf_12&c[]=cf_13&c[]=parent&per_page=100&group_by=status&sort=cf_12%2Ccf_3%2Csubject&utf8=%E2%9C%93&set_filter=1&f[]=cf_14&op[cf_14]=%3D&v[cf_14][]=';
    const staffBaseUrl = 'http://staff.scand';
    const workerStaffTargte = staffBaseUrl + '/issues?utf8=%E2%9C%93&set_filter=1&f[]=tracker_id&op[tracker_id]=%3D&v[tracker_id][]=4&f[]=cf_6&op[cf_6]=%3D&f[]=&c[]=status&c[]=cf_6&c[]=subject&c[]=assigned_to&c[]=cf_1&c[]=due_date&group_by=project&v[cf_6][]=';

    const rmHelper = new function() {
        this.formatDateISO = function(date) {
            let year = '' + date.getFullYear();
            let month = ( date.getMonth() + 1 > 9 ? '' : '0' ) + ( date.getMonth() + 1 );
            let day = ( date.getDate() > 9 ? '' : '0' ) + date.getDate();
            return [year, month, day].join('-');
        }
        this.getCustomFieldValueTD = function(fieldId) {
            let targetTd = null;
            let tds = document.getElementsByClassName(fieldId);
            if ( tds && tds.length > 1) {
                targetTd = tds[1];
            }
            return targetTd;
        }
        this.getCustomFieldValue = function(fieldId) {
            let targetId = this.getCustomFieldValueTD(fieldId);
            return ( targetId ? targetId.innerText : null );
        }
        this.getCustomerIvoiceRatesURL = function(customerId) {
            let lastMonthStart = this.formatDateISO(new Date(new Date(new Date().setMonth(new Date().getMonth() - 1)).setDate(1)));
            let lastMonthEnd = this.formatDateISO(new Date(new Date(new Date(new Date().setMonth(new Date().getMonth())).setDate(1)).setDate(0)));
            let resultURL = customerIvoiceRatesTarget.replace('${dateStart}', lastMonthStart).replace('${dateEnd}', lastMonthEnd) + customerId;
            return resultURL;
        }
    }

    if (window.location.href.indexOf(invBaseUrl + "/issues/") === 0) {
        // adding links to all Worker's rates
        let workerTD = rmHelper.getCustomFieldValueTD(nickFieldId);
        if ( workerTD ) {
            let workerId = rmHelper.getCustomFieldValue(nickFieldId);
            workerTD.innerHTML =
                "<a href='" + workerStaffTargte + workerId + "'>" + workerId + "</a> (" +
                "<a href='" + workerRatesTarget + workerId + "'>rates</a>)";
        }

        // adding links to all Customer's rates
        let customerTD = rmHelper.getCustomFieldValueTD(customerFieldId);
        if ( customerTD ) {
            let customerId = rmHelper.getCustomFieldValue(customerFieldId);
            customerTD.innerHTML = customerTD.innerHTML +
                " (<a href='" + customerRatesTarget + customerId + "'>rates</a>" +
                ", <a href='" + rmHelper.getCustomerIvoiceRatesURL(customerId) + "'>i-rates</a>)";
        }
    }
})();
